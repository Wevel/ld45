﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType
{
	public readonly string name;
	public readonly char floorChar;
	public readonly int healthStat;
	public readonly int speedStat;
	public readonly int strengthStat;
	public readonly int intelligenceStat;
	public readonly AttackType attackType;
	public readonly uint goldPerLevel;
	public readonly uint xpPerLevel;
	public readonly EnemyAI ai;
	public readonly int minLevel;

	public EnemyType (string name, char floorChar, int healthStat, int speedStat, int strengthStat, int intelligenceStat, AttackType attackType, uint goldPerLevel, uint xpPerLevel, EnemyAI ai, int minLevel)
	{
		this.name = name;
		this.floorChar = floorChar;
		this.healthStat = healthStat;
		this.speedStat = speedStat;
		this.strengthStat = strengthStat;
		this.intelligenceStat = intelligenceStat;
		this.attackType = attackType;
		this.goldPerLevel = goldPerLevel;
		this.xpPerLevel = xpPerLevel;
		this.ai = ai;
		this.minLevel = minLevel;
	}

	public Enemy CreateEnemy (Map map, uint level)
	{
		return new Enemy (map, name, healthStat, speedStat, strengthStat, intelligenceStat, floorChar, level, attackType, level * goldPerLevel, level * xpPerLevel, ai);
	}
}
