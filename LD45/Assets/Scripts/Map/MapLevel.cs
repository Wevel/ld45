﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapLevel
{
	public readonly int width;
	public readonly int height;
	public readonly int spawnX;
	public readonly int spawnY;
	public readonly bool canPan;
	public readonly string[] data;
	public readonly EnemyType[] enemyTypes;

	public MapLevel (int width, int height, int spawnX, int spawnY, bool canPan, string[] data, EnemyType[] enemyTypes)
	{
		this.width = width;
		this.height = height;
		this.spawnX = spawnX;
		this.spawnY = spawnY;
		this.canPan = canPan;
		this.data = data;
		this.enemyTypes = enemyTypes;
	}
}
