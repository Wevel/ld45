﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
	public readonly AttackType attackType;
	public readonly uint goldDrop;
	public readonly uint xpDrop;
	public EnemyAI ai;

	public bool canAttack = true;

	public int avaliableGameTicks;

	public Enemy (Map map, string name, int healthStat, int speedStat, int strengthStat, int intelligenceStat, char floorChar, uint level, AttackType attackType, uint goldDrop, uint xpDrop, EnemyAI ai)
		: base (map, name, healthStat, speedStat, strengthStat, intelligenceStat, floorChar, level)
	{
		this.attackType = attackType;
		this.goldDrop = goldDrop;
		this.xpDrop = xpDrop;
		this.ai = ai;
		avaliableGameTicks = 0;
		canAttack = true;
	}

	public override void UpdateTick ()
	{
		avaliableGameTicks++;
		ai.UpdateTick (this);
	}

	public override void OnDeath ()
	{
		map.AddItem (position.x, position.y, 'g', (item) => 
		{
			map.player.AddGold (goldDrop);
			map.LogMessage ($"{map.player.sentenceStartName} picked up {goldDrop} gold");
		});

		map.player.AddXp (xpDrop);
	}

	public override bool IsTarget (Character other)
	{
		return other is Player;
	}
}
