﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate int GetTickCount (Player player);
public delegate bool TryDoAbility (Player player);
public delegate bool CanDoAbility (Player player);
public delegate void DoneAbility (Player player);

public class Ability
{
	public const int BaseMoveTickCount = 10;
	public const int BaseAttackTickCount = 15;

	public static Ability MoveUp = new Ability ("Move Up", 
		(player) => { return player.GetAbilityTickCount (BaseMoveTickCount, player.speedStat); },
		(player) => { return player.TryMoveTo (player.position.up); },
		(player) => { return true; },
		(player) => { player.visible = true; });

	public static Ability MoveLeft = new Ability ("Move Left",
		(player) => { return player.GetAbilityTickCount (BaseMoveTickCount, player.speedStat); },
		(player) => { return player.TryMoveTo (player.position.left); },
		(player) => { return true; },
		(player) => 
		{
			player.xpEnabled = true;
		});

	public static Ability MoveDown = new Ability ("Move Down",
		(player) => { return player.GetAbilityTickCount (BaseMoveTickCount, player.speedStat); },
		(player) => { return player.TryMoveTo (player.position.down); },
		(player) => { return true; },
		(player) => 
		{
			player.healthEnabled = true;
			for (int i = 0; i < 5; i++) player.map.SpawnEnemyAroundPlayer ("bat", 1, 8, 20);
		});

	public static Ability MoveRight = new Ability ("Move Right",
		(player) => { return player.GetAbilityTickCount (BaseMoveTickCount, player.speedStat); },
		(player) => { return player.TryMoveTo (player.position.right); },
		(player) => { return true; },
		(player) => 
		{
			player.meleeAttacksEnabled = true;
		});

	public static Ability MeleeAttack = new Ability ("Melee Attack",
		(player) => { return player.GetAttackTickCount (BaseMoveTickCount, AttackType.Magic); },
		(player) => { return player.TryAttack (AttackType.Melee); },
		(player) => { return player.CanAttack (AttackType.Melee); },
		(player) => 
		{
			player.itemsEnabled = true;
		});

	public static Ability RangedAttack = new Ability ("Ranged Attack",
		(player) => { return player.GetAttackTickCount (BaseMoveTickCount, AttackType.Magic); },
		(player) => { return player.TryAttack (AttackType.Ranged); },
		(player) => { return player.CanAttack (AttackType.Ranged); },
		(player) => { });

	public static Ability MagicAttack = new Ability ("Magic Attack",
		(player) => { return player.GetAttackTickCount (BaseMoveTickCount, AttackType.Magic); },
		(player) => { return player.TryAttack (AttackType.Magic); },
		(player) => { return player.CanAttack (AttackType.Magic); },
		(player) => { });

	public static Ability PickUpItem = new Ability ("Pickup Item",
		(player) => { return 2; },
		(player) => { return player.map.TryPickUpFrom (player.position); },
		(player) => { return player.map.CanPickUpFrom (player.position); },
		(player) => { });

	public static Ability RegenHealth = new Ability ("Regen Health",
		(player) => { return 15; },
		(player) => { player.RegenHealth (); return true; },
		(player) => { return true; },
		(player) => {  });

	public static Ability OpenDoor = new Ability ("Use Door",
		(player) => { return 4; },
		(player) => { return player.map.TryUseDoorFrom (player.position); },
		(player) => { return player.map.CanUseDoorFrom (player.position); },
		(player) => {  });

	public readonly string name;
	public readonly GetTickCount getTickCount;
	public readonly TryDoAbility tryDoAbility;
	public readonly CanDoAbility canDoAbility;
	public readonly DoneAbility doneAbility;

	public Ability (string name, GetTickCount getTickCount, TryDoAbility tryDoAbility, CanDoAbility canDoAbility, DoneAbility doneAbility)
	{
		this.name = name;
		this.getTickCount = getTickCount;
		this.tryDoAbility = tryDoAbility;
		this.canDoAbility = canDoAbility;
		this.doneAbility = doneAbility;
	}

}
