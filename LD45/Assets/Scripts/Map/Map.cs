﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map
{
	private const char openDoorVertical = '┊';
	private const char openDoorHorozontal = '┈';

	#region MapLevels

	public static List<MapLevel> levels = new List<MapLevel> ();
	public static char[] doorChars = new char[] { '┄', '┆' };
	public static char[] wallChars = new char[] { '━', '┃', '┏', '┓', '┗', '┛', '┣', '┫', '┳', '┻', '╋' };
	private readonly HashSet<char> doorsCharsSet = new HashSet<char> (doorChars);
	private readonly HashSet<char> wallsCharsSet = new HashSet<char> (wallChars);

	static Map ()
	{
		// ┄┆
		// ━┃┏┓┗┛┣┫┳┻╋

		addLevel (39, 10, false, new string[]
			{
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			"                                                                               ",
			},
			new EnemyType ("bat", 'b', 8, 12, 4, 4, AttackType.Melee, 1, 1, EnemyAI.randomWalk, 1),
			new EnemyType ("slime", 's', 10, 12, 8, 4, AttackType.Melee, 1, 1, EnemyAI.gotoPlayer, 2),
			new EnemyType ("goblin", 'n', 10, 10, 10, 5, AttackType.Melee, 2, 2, EnemyAI.gotoPlayer, 2),
			new EnemyType ("orc", 'o', 13, 7, 12, 6, AttackType.Melee, 5, 3, EnemyAI.gotoPlayer, 3));

		addLevel (8, 44, true, new string[]
		{
			"                                                                                                    ",
			"                                                                                                    ",
			"                                                                        ┏━━━━━━━━━━━━━━━━━━━━━━┓    ",
			"    ┏━━━━━━━┓                          ┏━━━━━━━━━┓                      ┃                      ┃    ",
			"    ┃       ┃                          ┃         ┃                      ┃                      ┃    ",
			"    ┃       ┃                          ┃         ┗━━━━━━━┓              ┃                      ┃    ",
			"    ┃       ┃                          ┃                 ┣━━━━━━━━━━━━━━┫                      ┃    ",
			"    ┃       ┃                          ┃                 ┆              ┆                      ┃    ",
			"    ┃       ┃                          ┃                 ┣━━━━━━━━━━━━━━┫                      ┃    ",
			"    ┃       ┃┏━━━━━━━━━━┓            ┏━┛                 ┃              ┣━━━━━━━━━━━━━         ┃    ",
			" ┏━━┛       ┗┛          ┃            ┃                   ┃              ┃                      ┃    ",
			" ┃                      ┃            ┃                   ┃              ┃                      ┃    ",
			" ┃                      ┃           ┏┛                   ┃              ┃                      ┃    ",
			" ┃                      ┃           ┃           ━━━━━━━━━┫              ┃                      ┃    ",
			" ┃                      ┃         ┏━┛                    ┃              ┃                      ┃    ",
			" ┃                      ┣━━━┓     ┃                      ┃              ┃                      ┃    ",
			" ┃                      ┃   ┃     ┃                      ┃              ┗━━━━━━━┳┄┳━━━┓        ┃    ",
			" ┃                      ┆   ┗━━┳━━┛                      ┃                      ┃ ┃   ┃        ┃    ",
			" ┣━━━━━━━━━━━━━━━━━━━━━━┛      ┆                  ┏━━━━━━┛                      ┃ ┃   ┗━━━━━━━━┛    ",
			" ┃                          ┏━━┻━━┓               ┃                             ┃ ┃                 ",
			" ┃                          ┃     ┗━┓             ┃                             ┃ ┃                 ",
			" ┃                          ┃    ┏━━┛             ┃                             ┃ ┃                 ",
			" ┃                          ┃    ┃              ┏━┛                             ┃ ┃                 ",
			" ┃                          ┃    ┃              ┗━━━━━━┓                        ┃ ┃                 ",
			" ┃                          ┃    ┃                     ┃             ┏━━━━━━━━━━┻┄┻━━┓              ",
			" ┗━━━━━┓                    ┃    ┃        ┃            ┃             ┃               ┃              ",
			"       ┃             ┏━━━━━━┛    ┃        ┃            ┃             ┃               ┃              ",
			"       ┃             ┗━━━┓       ┃        ┃            ┃             ┃               ┃              ",
			"       ┃                 ┃       ┃        ┃            ┃             ┃               ┗━━━━┓         ",
			"       ┃             ┏━┓ ┃       ┗━━━━━━━━┃            ┃             ┃                    ┃         ",
			"       ┗━━━━━━━━━━━━━┫ ┗┄┻━━━┓            ┃            ┃             ┃                    ┃         ",
			"                     ┃       ┃            ┃            ┃             ┃                    ┃         ",
			"                     ┃       ┃            ┃            ┣━━━━━━━━━━━━━┫                    ┃         ",
			"                     ┃       ┃            ┃            ┆             ┆                    ┃         ",
			"  ┏━━━━━━━━┓         ┃       ┃            ┃            ┣━━━━━━━━━━━━━┫                    ┃         ",
			"  ┃        ┃┏━━━━━━━━┛       ┃            ┃            ┃             ┃                    ┃         ",
			"  ┃        ┃┃                ┃            ┃            ┃             ┗━━━━━━━━━━━┓        ┃         ",
			"  ┃        ┃┃                ┗┳━━━━━━━━━━━┫            ┃                         ┃        ┃         ",
			"  ┃        ┃┃                 ┆           ┆            ┃                         ┃        ┃         ",
			"  ┃        ┣┛                ┏┻━━━━━━━━━━━┻━━━━━━━━━━━━┛                         ┃        ┃         ",
			"  ┃        ┆                 ┃                                                   ┃        ┃         ",
			"  ┃        ┣┓             ┏━━┛                                                   ┃        ┃         ",
			"  ┃        ┃┃             ┃                                                      ┗━━━━━━━━┛         ",
			"  ┃        ┃┃             ┃                                                                         ",
			"  ┃        ┃┃             ┃                                                                         ",
			"  ┃        ┃┃             ┃                                                                         ",
			"  ┗━━━━━━━━┛┃             ┃                                                                         ",
			"            ┃             ┃                                                                         ",
			"            ┗━━━━━━━━━━━━━┛                                                                         ",
			"                                                                                                    ",
		},
		new EnemyType ("slime", 's', 10, 12, 8, 4, AttackType.Melee, 1, 1, EnemyAI.gotoPlayer, 2),
		new EnemyType ("goblin", 'n', 10, 10, 10, 5, AttackType.Melee, 2, 1, EnemyAI.gotoPlayer, 2),
		new EnemyType ("orc", 'o', 13, 7, 12, 6, AttackType.Melee, 5, 1, EnemyAI.gotoPlayer, 3),
		new EnemyType ("troll", 't', 15, 4, 14, 5, AttackType.Melee, 10, 1, EnemyAI.gotoPlayer, 6),
		new EnemyType ("baby dragon", 'd', 15, 6, 10, 8, AttackType.Melee, 10, 2, EnemyAI.gotoPlayer, 8),
		new EnemyType ("dragon", 'D', 17, 10, 16, 10, AttackType.Melee, 20, 3, EnemyAI.gotoPlayer, 10),
		new EnemyType ("animated armor", 'a', 12, 10, 8, 8, AttackType.Melee, 6, 1, EnemyAI.gotoPlayer, 7),
		new EnemyType ("chimera", 'c', 15, 10, 12, 12, AttackType.Melee, 12, 2, EnemyAI.gotoPlayer, 9));
	}

	private static void addLevel (int spawnX, int spawnY, bool canPan, string[] data, params EnemyType[] enemyTypes)
	{
		int width = 0;
		for (int i = 0; i < data.Length; i++)
		{
			if (data[i].Length > width) width = data[i].Length;
		}

		levels.Add (new MapLevel (width, data.Length, spawnX, spawnY, canPan, data, enemyTypes));
	}

	#endregion

	public readonly Game game;

	public readonly int width;
	public readonly int height;
	public readonly MapLevel level;
	public readonly float charScrollDelay = 0.75f;

	public Player player { get; private set; }

	public Tile this[int x, int y]
	{
		get
		{
			if (x >= 0 && x < width && y >= 0 && y < height) return tiles[x, y];
			else return null;
		}
	}

	private readonly Tile[,] tiles;
	private readonly List<Character> characters = new List<Character> ();
	private readonly List<char> itemStackChars = new List<char> ();
	private readonly Dictionary<string, EnemyType> enemyTypes = new Dictionary<string, EnemyType> ();

	public Map (Game game, MapLevel level)
	{
		this.game = game;
		this.level = level;
		this.width = level.width;
		this.height = level.height;

		for (int i = 0; i < level.enemyTypes.Length; i++) enemyTypes.Add (level.enemyTypes[i].name, level.enemyTypes[i]);

		tiles = new Tile[width, height];

		char c;
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (x < level.data[height - y - 1].Length) c = level.data[height - y - 1][x];
				else c = ' ';

				tiles[x, y] = new Tile (x, y, this, wallsCharsSet.Contains (c), doorsCharsSet.Contains (c), c);
			}
		}

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				tiles[x, y].GenerateNeighbours ();
			}
		}

		List<Tile> open = new List<Tile> ();
		List<Tile> closed = new List<Tile> ();
		open.Add (tiles[level.spawnX, level.spawnY]);
		Tile current, tmp; ;

		while (open.Count > 0)
		{
			current = open[open.Count - 1];
			open.RemoveAt (open.Count - 1);
			closed.Add (current);
			current.accessible = true;

			if ((tmp = current.right) != null && !tmp.accessible && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
			if ((tmp = current.up) != null && !tmp.accessible && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
			if ((tmp = current.left) != null && !tmp.accessible && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
			if ((tmp = current.down) != null && !tmp.accessible && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
		}
		
		List<Tile> remaining = new List<Tile> (closed);
		int room = 1;

		open.Clear ();
		closed.Clear ();


		while (remaining.Count > 0)
		{
			current = remaining[remaining.Count - 1];
			remaining.RemoveAt (remaining.Count - 1);
			current.roomNumber = room;
			open.Add (current);

			while (open.Count > 0)
			{
				current = open[open.Count - 1];
				open.RemoveAt (open.Count - 1);
				remaining.Remove (current);
				closed.Add (current);

				if (!current.isDoor)
				{
					current.roomNumber = room;

					if ((tmp = current.right) != null && tmp.roomNumber == 0 && !tmp.isDoor && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
					if ((tmp = current.up) != null && tmp.roomNumber == 0 && !tmp.isDoor && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
					if ((tmp = current.left) != null && tmp.roomNumber == 0 && !tmp.isDoor && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
					if ((tmp = current.down) != null && tmp.roomNumber == 0 && !tmp.isDoor && !closed.Contains (tmp) && !open.Contains (tmp) && !tmp.isWall) open.Add (tmp);
				}
			}

			room++;
		}
	}

	public bool TryPickUpFrom (Tile tile)
	{
		if (tile.items.Count == 0) return false;

		tile.items[0].Pickup ();
		return true;
	}

	public bool CanPickUpFrom (Tile tile)
	{
		return tile.items.Count > 0;
	}

	public bool TryUseDoorFrom (Tile tile)
	{
		bool done = false;
		if (tile.right != null && tile.right.isDoor && tile.right.TryUseDoor ()) done = true;
		if (tile.left != null && tile.left.isDoor && tile.left.TryUseDoor ()) done = true;
		if (tile.up != null && tile.up.isDoor && tile.up.TryUseDoor ()) done = true;
		if (tile.down != null && tile.down.isDoor && tile.down.TryUseDoor ()) done = true;
		return done;
	}

	public bool CanUseDoorFrom (Tile tile)
	{
		if (tile.right != null && tile.right.isDoor && tile.right.CanUseDoor ()) return true;
		if (tile.left != null && tile.left.isDoor && tile.left.CanUseDoor ()) return true;
		if (tile.up != null && tile.up.isDoor && tile.up.CanUseDoor ()) return true;
		if (tile.down != null && tile.down.isDoor && tile.down.CanUseDoor ()) return true;
		return false;
	}

	public void UpdateEnemyCount ()
	{
		if (characters.Count < 4 + player.level)
		{
			List<EnemyType> possible = new List<EnemyType> ();
			for (int i = 0; i < level.enemyTypes.Length; i++)
			{
				if (player.level >= level.enemyTypes[i].minLevel) possible.Add (level.enemyTypes[i]);
			}

			if (possible.Count > 0)
			{
				EnemyType type = possible[Random.Range (0, possible.Count)];

				if (level.canPan)
				{
					SpawnEnemyAroundPlayer (type, player.level, 8, 20);
				}
				else
				{
					int side = Random.Range (0, 4);
					if (side == 0) SpawnEnemy (type, player.level, this[0, Random.Range (0, height)]);
					else if (side == 1) SpawnEnemy (type, player.level, this[Random.Range (0, width), height - 1]);
					else if (side == 2) SpawnEnemy (type, player.level, this[width - 1, Random.Range (0, height)]);
					else SpawnEnemy (type, player.level, this[Random.Range (0, width), 0]);
				}
			}
			else Debug.LogWarning ($"No possible enemies to spawn");
		}
	}

	public void LogMessage (string message)
	{
		game.display.AddLogMessage (message);
	}

	public void AddItem (int x, int y, char floorChar, PickupItem pickupItem)
	{
		if (player != null && !player.itemsEnabled) return;
			 
		Tile t = this[x, y];
		if (t != null)
		{
			if (!t.isWall)
			{
				new Item (t, floorChar, pickupItem);
			}
			else Debug.LogWarning ($"Can't place item {floorChar} at [{t}] as it is a wall");
		}
		else Debug.LogWarning ($"Can't place item {floorChar} at [{x}, {y}] as it doens't exist");
	}

	public void SpawnEnemyAroundPlayer (string key, uint level, float minDist, float maxDist)
	{
		if (enemyTypes.TryGetValue (key, out EnemyType type)) SpawnEnemyAroundPlayer (type, level, minDist, maxDist);
		else Debug.LogWarning ($"No enemy type with key {key}");
	}

	public void SpawnEnemyAroundPlayer (EnemyType type, uint level, float minDist, float maxDist)
	{
		int remainigAttempts = 50;
		float dist;
		Vector2 offset;
		Tile t;

		while (remainigAttempts > 0)
		{
			dist = Random.Range (minDist, maxDist);
			offset = Random.insideUnitCircle.normalized * dist;
			t = this[player.position.x + Mathf.FloorToInt (offset.x), player.position.y + Mathf.FloorToInt (offset.y)];
			if (t != null && !t.isWall && !t.isDoor && t.accessible )
			{
				SpawnEnemy (type, level, t);
				return;
			}

			remainigAttempts--;
		}
	}

	public void SpawnEnemy (EnemyType type, uint level, Tile t)
	{
		AddCharacter (type.CreateEnemy (this, level), t);
	}

	public void AddCharacter (Character character, int x, int y)
	{
		AddCharacter (character, this[x, y]);
	}

	public void AddCharacter (Character character, Tile t)
	{
		if (t != null)
		{
			if (!t.isWall)
			{
				if (character.TryMoveTo (t))
				{
					if (!characters.Contains (character)) characters.Add (character);
				}
				else
				{
					Debug.LogWarning ($"Can't put character {character.floorChar} on tile {t}");
				}

				if (character is Player p && player == null) player = p;
			}
			else Debug.LogWarning ($"Can't place character {character.floorChar} at [{t}] as it is a wall");
		}
		else Debug.LogWarning ($"Can't place character {character.floorChar} at a null tile");
	}

	public void ResetEnemyAttackedMarker ()
	{
		for (int i = 0; i < characters.Count; i++)
		{
			if (characters[i] is Enemy enemy) enemy.canAttack = true;
		}
	}

	public void UpdateTick ()
	{
		for (int i = 0; i < characters.Count; i++)
		{
			if (characters[i].alive) characters[i].UpdateTick ();
		}

		for (int i = characters.Count - 1; i >= 0; i--)
		{
			if (!characters[i].alive) characters.RemoveAt (i);
		}
	}

	public char GetDisplayChar (int x, int y)
	{
		Tile t = this[x, y];
		if (t != null)
		{
			char baseFloorChar = getBaseFloorChar (t);
			
			if (t.currentCharacter != null && (player == null || t.currentCharacter != player || (t.currentCharacter == player && player.visible)))
			{
				return t.currentCharacter.floorChar;
			}
			else if (t.items.Count > 0)
			{
				if (player == null || !player.itemsEnabled) return baseFloorChar;
				else
				{
					itemStackChars.Clear ();
					itemStackChars.Add (baseFloorChar);
					for (int i = 0; i < t.items.Count; i++) itemStackChars.Add (t.items[i].floorChar);

					return itemStackChars[Mathf.FloorToInt (Time.time / charScrollDelay) % itemStackChars.Count];
				}
			}
			else return baseFloorChar;
		}
		else
		{
			return ' ';
		}
	}

	private char getBaseFloorChar (Tile t)
	{
		if (t.isDoor)
		{
			if (player != null && !player.doorsEnabled) return ' ';
			else if (t.isOpenDoor)
			{
				if (t.floorChar == '┆') return openDoorVertical;
				else if (t.floorChar == '┄') return openDoorHorozontal;
				else return t.floorChar;
			}
			else return t.floorChar;
		}
		else return t.floorChar;
	}
}
