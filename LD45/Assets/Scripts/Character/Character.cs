﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character
{
	public const int damageDiceType = 6;

	private static void doAttack (Character attacker, Character defender, AttackType type)
	{
		string attackText = "melee";
		switch (type)
		{
			case AttackType.Melee:
			default:
				attackText = "melee";
				break;
			case AttackType.Ranged:
				attackText = "ranged";
				break;
			case AttackType.Magic:
				attackText = "magic";
				break;
		}

		int damage = attacker.GetDamage (type);
		defender.Damage (damage);
		attacker.map.LogMessage ($"{attacker.sentenceStartName} used {attackText} to damage {defender.name} for {damage} damage");
	}

	public readonly string name;
	public readonly string sentenceStartName;

	public Map map { get; protected set; }
	public Tile position { get; private set; }

	public int maxHealth { get; protected set; }
	public int currentHealth { get; protected set; }

	public int healthStat;
	public int speedStat;
	public int strengthStat;
	public int intelligenceStat;

	public char floorChar;

	public uint level { get; protected set; }
	public bool alive { get; private set; }

	public Character (Map map, string name, int healthStat, int speedStat, int strengthStat, int intelligenceStat, char floorChar, uint level)
	{
		this.map = map;
		this.name = name;
		this.position = null;
		this.healthStat = healthStat;
		this.speedStat = speedStat;
		this.strengthStat = strengthStat;
		this.intelligenceStat = intelligenceStat;
		this.floorChar = floorChar;
		this.level = level;

		sentenceStartName = char.ToUpper (name[0]) + name.Substring (1);

		maxHealth = GetMaxHealth ();
		currentHealth = maxHealth;

		alive = true;
	}

	public void Damage (int amount)
	{
		currentHealth -= amount;
		if (currentHealth <= 0)
		{
			currentHealth = 0;
			position.RemoveCharacter (this);
			alive = false;

			OnDeath ();
		}
	}

	public int GetMaxHealth ()
	{
		return Mathf.Max (1, Mathf.FloorToInt ((float)level * 8f * Mathf.Exp (0.1f * ((float)healthStat - 9f))));
	}

	public int GetDamage (AttackType type)
	{
		int damage = 0;

		for (int i = 0; i < level; i++) damage += Random.Range (1, damageDiceType);

		switch (type)
		{
			case AttackType.Melee:
			default:
				damage += GetDamageModifier (strengthStat);
				break;
			case AttackType.Ranged:
				damage += GetDamageModifier (speedStat);
				break;
			case AttackType.Magic:
				damage += GetDamageModifier (intelligenceStat);
				break;
		}

		if (damage < 0) damage = 0;
		return damage;
	}

	public bool TryAttack (AttackType type)
	{
		if (CanAttackTile (position.right, type))
		{
			doAttack (this, position.right.currentCharacter, type);
			return true;
		}
		else if (CanAttackTile (position.up, type))
		{
			doAttack (this, position.up.currentCharacter, type);
			return true;
		}
		else if (CanAttackTile (position.left, type))
		{
			doAttack (this, position.left.currentCharacter, type);
			return true;
		}
		else if (CanAttackTile (position.down, type))
		{
			doAttack (this, position.down.currentCharacter, type);
			return true;
		}

		return false;
	}

	public bool CanAttack (AttackType type)
	{
		return CanAttackTile (position.right, type)
			|| CanAttackTile (position.up, type)
			|| CanAttackTile (position.left, type)
			|| CanAttackTile (position.down, type);
	}

	public bool CanAttackTile (Tile tile, AttackType type)
	{
		if (tile == null) return false;
		if (tile.currentCharacter == null) return false;
		if (!tile.currentCharacter.alive) return false;
		if (!IsTarget (tile.currentCharacter)) return false;
		return true;
	}

	public bool TryMoveTo (int x, int y)
	{
		Tile t = map[x, y];
		if (t != null) return TryMoveTo (t);
		else return false;
	}

	public bool TryMoveTo (Tile newTile)
	{
		if (newTile == null) return false;
		if (newTile.TryAddCharacter (this))
		{
			if (position != null) position.RemoveCharacter (this);
			position = newTile;
			return true;
		}

		return false;
	}

	public int GetAttackTickCount (int baseTickCount, AttackType type)
	{
		switch (type)
		{
			case AttackType.Melee:
			default:
				return GetAbilityTickCount (baseTickCount, Mathf.FloorToInt ((speedStat + strengthStat) / 2f));
			case AttackType.Ranged:
				return GetAbilityTickCount (baseTickCount, speedStat);
			case AttackType.Magic:
				return GetAbilityTickCount (baseTickCount, intelligenceStat);
		}
	}

	public int GetAbilityTickCount (int baseTickCount, int statValue)
	{
		return Mathf.Max (1, Mathf.FloorToInt ((float)baseTickCount * Mathf.Exp (-0.1f * ((float)statValue - 10.1f))));
	}

	public int GetDamageModifier (int statValue)
	{
		return Mathf.FloorToInt (0.5f * ((float)statValue - 9.9f));
	}

	public abstract void UpdateTick ();
	public abstract void OnDeath ();
	public abstract bool IsTarget (Character other);
}
