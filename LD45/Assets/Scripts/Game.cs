﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
	public Display display;
	public char playerChar = '@';

	public Map map { get; private set; }
	public Player player { get; private set; }
	private int currentLevelIndex = -1;

	public bool showStartScreen = true;

	private readonly List<Ability> abilities = new List<Ability> ();
	private readonly List<char> abilityKeys = new List<char> ();
	private readonly HashSet<char> usedKeys = new HashSet<char> ();

	public IEnumerable<KeyValuePair<char, string>> Abilities
	{
		get
		{
			for (int i = 0; i < abilities.Count; i++)
				yield return new KeyValuePair<char, string> (abilityKeys[i], abilities[i].name);
		}
	}


	private void Awake ()
	{
		display.SetMap (this, null);
	}
	
	void Start()
    {
		StartGame ();
		Sound.PlaySong ();
	}

    void Update()
    {
		if (map != null)
		{
			if (Input.anyKeyDown 
				&& !Input.GetMouseButtonDown (0) 
				&& !Input.GetMouseButtonDown (1) 
				&& !Input.GetMouseButtonDown (2) 
				&& !Input.GetMouseButtonDown (3) 
				&& !Input.GetMouseButtonDown (4))
			{
				if (showStartScreen)
				{
					StartGame ();
					showStartScreen = false;
				}
				else if (player.alive)
				{
					string inputString = Input.inputString;

					if (Input.GetKeyDown (KeyCode.UpArrow)) inputString += '↑';
					if (Input.GetKeyDown (KeyCode.DownArrow)) inputString += '↓';
					if (Input.GetKeyDown (KeyCode.RightArrow)) inputString += '→';
					if (Input.GetKeyDown (KeyCode.LeftArrow)) inputString += '←';
					inputString = inputString.Replace (' ', '_');

					if (inputString != "")
					{
						for (int c = 0; c < inputString.Length; c++)
						{
							for (int i = 0; i < abilityKeys.Count; i++)
							{
								if (abilityKeys[i] != (char)0)
								{
									if (abilityKeys[i] == inputString[c])
									{
										player.DoAbility (abilities[i]);
										continue;
									}
								}
								else
								{
									if (!usedKeys.Contains (inputString[c]))
									{
										abilityKeys[i] = inputString[c];
										usedKeys.Add (inputString[c]);
										player.DoAbility (abilities[i]);
										abilities[i].doneAbility (player);
										continue;
									}
								}
							}
						}
					}
				}
				else StartGame ();
			}

			if (player != null)
			{
				switch (player.level)
				{
					case 1:
						if (player.meleeAttacksEnabled) map.UpdateEnemyCount ();
						break;
					case 2:
						map.UpdateEnemyCount ();
						break;
					case 3:
						map.UpdateEnemyCount ();
						break;
					case 4:
						map.UpdateEnemyCount ();
						break;
					case 5:
						map.UpdateEnemyCount ();
						break;
					case 6:
						map.UpdateEnemyCount ();
						break;
					case 7:
						map.UpdateEnemyCount ();
						break;
					case 8:
						map.UpdateEnemyCount ();
						break;
					case 9:
					default:
						map.UpdateEnemyCount ();
						break;
				}
			}

			display.UpdateDisplay ();
		}
    }

	public void StartGame ()
	{
		currentLevelIndex = -1;
		initCharacter ();
		StartNextLevel ();
		Sound.PlayNextNote ();
	}

	public void StartNextLevel ()
	{
		currentLevelIndex++;

		if (currentLevelIndex < Map.levels.Count)
		{
			map = new Map (this, Map.levels[currentLevelIndex]);
			player.SetMap (map);

			map.AddCharacter (player, map.level.spawnX, map.level.spawnY);

			display.SetMap (this, map);
		}
		else
		{
			currentLevelIndex--;
			Debug.LogWarning ($"Already at max level {currentLevelIndex}");
		}
	}

	public void OnPlayerLevelUp ()
	{
		switch (player.level)
		{
			case 1:
				break;
			case 2:
				addAbility (Ability.PickUpItem);
				break;
			case 3:
				addAbility (Ability.RegenHealth);
				break;
			case 4:
				break;
			case 5:
				StartNextLevel ();
				break;
			case 6:
				player.doorsEnabled = true;
				addAbility (Ability.OpenDoor);
				break;
			case 7:
				break;
			case 8:
				break;
			case 9:
			default:
				break;
		}
	}

	private void initCharacter ()
	{
		player = new Player (map, "player", 11, 12, 13, 14, playerChar);
		usedKeys.Clear ();

		// We could partially randomise this
		// Clearly movement should still be the first 4, followed by some sort of attack
		abilities.Clear ();
		abilityKeys.Clear ();
		addAbility (Ability.MoveUp);
		addAbility (Ability.MoveLeft);
		addAbility (Ability.MoveDown);
		addAbility (Ability.MoveRight);
		addAbility (Ability.MeleeAttack);
	}

	private void addAbility (Ability ability)
	{
		abilities.Add (ability);
		abilityKeys.Add ((char)0);
	}
}
