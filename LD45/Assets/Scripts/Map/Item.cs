﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void PickupItem (Item item);

public class Item
{
	public readonly Tile tile;
	public readonly char floorChar;
	public readonly PickupItem pickupItem;

	public Item (Tile tile, char floorChar, PickupItem pickupItem)
	{
		this.tile = tile;
		this.floorChar = floorChar;
		this.pickupItem = pickupItem;

		tile.AddItem (this);
	}

	public void Pickup ()
	{
		tile.RemoveItem (this);
		pickupItem (this);
	}
}
