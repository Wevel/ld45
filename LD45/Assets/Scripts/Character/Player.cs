﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
	public uint gold { get; private set; }
	public uint xp { get; private set; }
	public uint xpTarget { get; private set; }

	public bool doorsEnabled = false;
	public bool itemsEnabled = false;
	public bool statsEnabled = false;
	public bool meleeAttacksEnabled = false;
	public bool xpEnabled = false;
	public bool healthEnabled = false;
	public bool visible = false;

	public Player (Map map, string name, int healthStat, int speedStat, int strengthStat, int intelligenceStat, char floorChar)
		: base (map, name, healthStat, speedStat, strengthStat, intelligenceStat, floorChar, 1)
	{
		gold = 0;
		xp = 0;
		xpTarget = GetXPTarget (1);
	}

	public void SetMap (Map map)
	{
		this.map = map;
	}

	public void DoAbility (Ability ability)
	{
		if (ability == null) return;
		if (ability.tryDoAbility (this))
		{
			Sound.PlayNextNote ();
			int tickCount = ability.getTickCount (this);
			for (int i = 0; i < tickCount; i++) map.UpdateTick ();

			map.ResetEnemyAttackedMarker ();
		}
	}

	public void AddGold (uint amount)
	{
		if (itemsEnabled) gold += amount;
	}

	public void AddXp (uint amount)
	{
		if (xpEnabled)
		{
			xp += amount;
			if (xp >= xpTarget) LevelUp ();
		}
	}

	public void RegenHealth ()
	{
		if (healthEnabled)
		{
			currentHealth = currentHealth + (int)level;
			if (currentHealth > maxHealth) currentHealth = maxHealth;
		}
	}

	public void LevelUp ()
	{
		level++;
		xpTarget = GetXPTarget (level);

		int newMaxHealth = GetMaxHealth ();
		int healthIncreace = newMaxHealth - maxHealth;
		currentHealth += healthIncreace;
		maxHealth = newMaxHealth;

		map.game.OnPlayerLevelUp ();
	}

	public uint GetXPTarget (uint l)
	{
		return (uint)Mathf.Max (1, Mathf.FloorToInt (1.5f * ((float)l + 0.05f)));
	}

	public override void UpdateTick () { }

	public override void OnDeath ()
	{
		Sound.PlaySong ();
		Debug.Log ("F");
	}

	public override bool IsTarget (Character other)
	{
		return other is Enemy;
	}
}
