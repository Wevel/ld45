﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MapGenerator : MonoBehaviour
{
	public int width = 100;
	public int height = 100;
	public int minRoomSize = 10;
	public int maxRoomSize = 20;
	public int maxRooms = 10;
	public string output;

	public void Generate ()
	{
		char[,] map = new char[width, height];
		bool[,] used = new bool[width, height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				map[x, y] = ' ';
				used[x, y] = false;
			}
		}

		int startX, startY, roomWidth, roomHeight;
		for (int i = 0; i < maxRooms; i++)
		{
			roomWidth = Random.Range (minRoomSize, maxRoomSize + 1);
			roomHeight = Random.Range (minRoomSize, maxRoomSize + 1);
			startX = Random.Range (2, width - roomWidth - 2);
			startY = Random.Range (2, height - roomHeight - 2);

			for (int x = 0; x < roomWidth; x++)
			{
				for (int y = 0; y < roomHeight; y++)
				{
					used[startX + x, startY + y] = true;
				}
			}
		}

		for (int x = 1; x < width - 1; x++)
		{
			for (int y = 1; y < height - 1; y++)
			{
				if (!used[x, y])
				{
					if (used[x + 1, y] || used[x - 1, y]) map[x, y] = '┃';
					else if (used[x, y + 1] || used[x, y - 1]) map[x, y] = '━';
				}
			}
		}

		StringBuilder sb = new StringBuilder ();
		for (int y = height - 1; y >= 0; y--)
		{
			sb.Append ("\"");
			for (int x = 0; x < width; x++) sb.Append (map[x, y]);
			sb.Append ("\",\r\n");
		}

		output = sb.ToString ();
	}
}

#if UNITY_EDITOR

[CustomEditor (typeof (MapGenerator))]
public class MapGeneratorEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		if (GUILayout.Button ("Generate")) ((MapGenerator)target).Generate ();
	}
}

#endif