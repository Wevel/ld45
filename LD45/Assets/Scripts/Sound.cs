﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
	private const string song = "2,564,756,34,54,123,25,3,013,543,475,320,567,32,2,564,756,34,54,123,25,3,013,543";

	public static Sound instance;

	private static readonly int[] songNotes;

	static Sound ()
	{
		songNotes = new int[song.Length];
		for (int i = 0; i < song.Length; i++)
		{
			if (int.TryParse (song[i] + "", out int value)) songNotes[i] = value;
			else songNotes[i] = -1;
		}
	}

	public static void PlayNextNote ()
	{
		instance.playNextNote ();
	}

	public static void PlaySong ()
	{
		instance.playSong = true;
	}

	public AudioClip pauseAudioClip;
	public AudioSource musicAudioSource;
	public AudioClip[] notes;
	private int nextNote = 0;

	private Queue<AudioClip> notesQueue = new Queue<AudioClip> ();
	private bool playSong;

	private void OnEnable ()
	{
		if (instance == null) instance = this;
		nextNote = 0;
	}

	private void OnDisable ()
	{
		if (instance == this) instance = null;
	}

	private void Update ()
	{
		if (playSong)
		{
			if (!musicAudioSource.isPlaying)
			{
				if (songNotes[nextNote] < 0) musicAudioSource.PlayOneShot (pauseAudioClip);
				else musicAudioSource.PlayOneShot (notes[songNotes[nextNote]]);
				nextNote = (nextNote + 1) % songNotes.Length;
			}
		}
		else
		{
			if (notesQueue.Count > 0)
			{
				if (!musicAudioSource.isPlaying) musicAudioSource.PlayOneShot (notesQueue.Dequeue ());
			}
		}
	}

	private void playNextNote ()
	{
		if (notesQueue.Count > 0) notesQueue.Dequeue ();
		if (songNotes[nextNote] < 0) notesQueue.Enqueue (pauseAudioClip);
		else notesQueue.Enqueue (notes[songNotes[nextNote]]);
		nextNote = (nextNote + 1) % songNotes.Length;

		playSong = false;
	}
}
