﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAI
{
	public static RandomWalk randomWalk = new RandomWalk ();
	public static GotoPlayer gotoPlayer = new GotoPlayer ();

	public class RandomWalk : EnemyAI
	{
		public override void UpdateTick (Enemy enemy)
		{
			if (enemy.CanAttack (enemy.attackType))
			{
				int attackCost = enemy.GetAttackTickCount (Ability.BaseAttackTickCount, enemy.attackType);
				if (enemy.avaliableGameTicks >= attackCost)
				{
					if (enemy.TryAttack (enemy.attackType))
					{
						enemy.avaliableGameTicks -= attackCost;
						enemy.canAttack = false;
					}
				}
			}
			else
			{
				int moveCost = enemy.GetAbilityTickCount (Ability.BaseMoveTickCount, enemy.speedStat);
				if (enemy.avaliableGameTicks >= moveCost)
				{
					// ToDo pathfinding
					List<Tile> possibleTargets = new List<Tile> ();
					Tile tmp;
					if ((tmp = enemy.position.right) != null && tmp.currentCharacter == null) possibleTargets.Add (tmp);
					if ((tmp = enemy.position.up) != null && tmp.currentCharacter == null) possibleTargets.Add (tmp);
					if ((tmp = enemy.position.left) != null && tmp.currentCharacter == null) possibleTargets.Add (tmp);
					if ((tmp = enemy.position.down) != null && tmp.currentCharacter == null) possibleTargets.Add (tmp);

					int i;
					while (possibleTargets.Count > 0)
					{
						i = Random.Range (0, possibleTargets.Count);
						if (enemy.TryMoveTo (possibleTargets[i]))
						{
							enemy.avaliableGameTicks -= moveCost;
							break;
						}
						else possibleTargets.RemoveAt (i);
					}
				}
			}
		}
	}

	public class GotoPlayer : EnemyAI
	{
		public override void UpdateTick (Enemy enemy)
		{
			if (enemy.CanAttack (enemy.attackType))
			{
				int attackCost = enemy.GetAttackTickCount (Ability.BaseAttackTickCount, AttackType.Melee);
				if (enemy.avaliableGameTicks >= attackCost)
				{
					if (enemy.TryAttack (enemy.attackType))
					{
						enemy.avaliableGameTicks -= attackCost;
						enemy.canAttack = false;
					}
				}
			}
			else
			{
				int moveCost = enemy.GetAbilityTickCount (Ability.BaseMoveTickCount, enemy.speedStat);
				if (enemy.avaliableGameTicks >= moveCost)
				{
					// ToDo pathfinding

					if (enemy.position.roomNumber == enemy.map.player.position.roomNumber || enemy.position.DistanceTo (enemy.map.player.position) < 6f)
					{
						int dx = enemy.map.player.position.x - enemy.position.x;
						int dy = enemy.map.player.position.y - enemy.position.y;
						if (dx == 0 && dy == 0)
						{
							// Not sure how, but we are on the same tile as the player
							if (enemy.TryMoveTo (enemy.position.right)
								|| enemy.TryMoveTo (enemy.position.up)
								|| enemy.TryMoveTo (enemy.position.left)
								|| enemy.TryMoveTo (enemy.position.down)) enemy.avaliableGameTicks -= moveCost;
							else Debug.LogWarning ($"Enemy {enemy.floorChar} is on same tile as player and cant move off");
						}
						else if (Mathf.Abs (dx) > Mathf.Abs (dy))
						{
							if (dx > 0)
							{
								if (enemy.TryMoveTo (enemy.position.right)) enemy.avaliableGameTicks -= moveCost;
							}
							else
							{
								if (enemy.TryMoveTo (enemy.position.left)) enemy.avaliableGameTicks -= moveCost;
							}
						}
						else
						{
							if (dy > 0)
							{
								if (enemy.TryMoveTo (enemy.position.up)) enemy.avaliableGameTicks -= moveCost;
							}
							else
							{
								if (enemy.TryMoveTo (enemy.position.down)) enemy.avaliableGameTicks -= moveCost;
							}
						}
					}
				}
			}
		}
	}

	public abstract void UpdateTick (Enemy enemy);
}
