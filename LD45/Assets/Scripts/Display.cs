﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;

public class Display : MonoBehaviour
{
	private const string newLine = "\r\n";
	private const int controlsCharX = 83;
	private const int controlsNameX = 88;

	private static readonly string[] baseUI = new string[]
	{
		"╔═══════════════════════════════════════════════════════════════════════════════╦═════════════════════════════╗",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"║                                                                               ║                             ║",
		"╠═══════════════════════════════════════════════════════════════════════════════╣                             ║",
		"║                                                                               ╠══════════════╦══════════════╣",
		"║                                                                               ║  CON     00  ║  DEX     00  ║",
		"║                                                                               ╠══════════════╬══════════════╣",
		"║                                                                               ║  STR     00  ║  INT     00  ║",
		"║                                                                               ╠══════════════╬══════════════╣",
		"║                                                                               ║ Health 0000  ║ Gold   0000  ║",
		"║                                                                               ╠══════════════╬══════════════╣",
		"║                                                                               ║  XP    0000  ║ Level  0000  ║",
		"╚═══════════════════════════════════════════════════════════════════════════════╩══════════════╩══════════════╝",
	};

	//     ______                 _              
	//    |  ____|               | |             
	//    | |__   _ __ ___  _ __ | |_ _   _      
	//    |  __| | '_ ` _ \| '_ \| __| | | |     
	//    | |____| | | | | | |_) | |_| |_| |     
	// ___|______|_| |_| |_| .__/ \__|\__, |     
	//|  __ \              | |         __/ |     
	//| |  | |_   _ _ __   |_|_  ___  |___/_ __  
	//| |  | | | | | '_ \ / _` |/ _ \/ _ \| '_ \ 
	//| |__| | |_| | | | | (_| |  __/ (_) | | | |
	//|_____/ \__,_|_| |_|\__, |\___|\___/|_| |_|
	//                     __/ |                 
	//                    |___/                  

	private static readonly string[] startScreen = new string[]
	{
		@"╔═════════════════════════════════════════════════════════════════════════════════════════════════════════════╗",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                       ______                 _                                              ║",
		@"║                                      |  ____|               | |                                             ║",
		@"║                                      | |__   _ __ ___  _ __ | |_ _   _                                      ║",
		@"║                                      |  __| | '_ ` _ \| '_ \| __| | | |                                     ║",
		@"║                                      | |____| | | | | | |_) | |_| |_| |                                     ║",
		@"║                                   ___|______|_| |_| |_| .__/ \__|\__, |                                     ║",
		@"║                                  |  __ \              | |         __/ |                                     ║",
		@"║                                  | |  | |_   _ _ __   |_|_  ___  |___/_ __                                  ║",
		@"║                                  | |  | | | | | '_ \ / _` |/ _ \/ _ \| '_ \                                 ║",
		@"║                                  | |__| | |_| | | | | (_| |  __/ (_) | | | |                                ║",
		@"║                                  |_____/ \__,_|_| |_|\__, |\___|\___/|_| |_|                                ║",
		@"║                                                       __/ |                                                 ║",
		@"║                                                      |___/                                                  ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                                                                                             ║",
		@"║                                            PRESS ANY KEY TO START                                           ║",
		@"║                                                                                                             ║",
		@"╚═════════════════════════════════════════════════════════════════════════════════════════════════════════════╝",
	};

	private static readonly string[] gameOverUI = new string[]
	{
		"╔═════════════════════╗",
		"║     ╔═════════╗     ║",
		"║     ║GAME OVER║     ║",
		"║     ╚═════════╝     ║",
		"║                     ║",
		"║     Gold    9999    ║",
		"║       XP    9999    ║",
		"║    Level    9999    ║",
		"║                     ║",
		"║  PRESS ANY KEY TO   ║",
		"║       RESTART       ║",
		"║                     ║",
		"╚═════════════════════╝",
	};

	public int width = 111;
	public int height = 31;
	public int mapAreaWidth = 78;
	public int mapAreaHeight = 20;
	public int playerPositionX = 39;
	public int playerPositionY = 10;
	public int gameOverUIWidth = 23;
	public int gameOverUIHeight = 13;
	public int logLineCount = 8;
	public int maxLogLineLength = 77;
	public Color color = Color.white;
	public Text displayText;
	public PostProcessVolume postProcessing;

	private StringBuilder outputTextBuilder = new StringBuilder ();
	private char[,] screenBuffer;
	private string[] logMessages;

	private Game game;
	private Map map;

	private void Awake ()
	{
		SetColour (color);
		SetScreenSize (width, height);

		logMessages = new string[8];
	}

	void Update()
    {
		UpdateDisplay ();
	}

	public void SetMap (Game game, Map map)
	{
		this.game = game;
		this.map = map;
		UpdateDisplay ();
		logMessages = new string[logLineCount];
	}

	public void UpdateDisplay ()
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				screenBuffer[x, y] = ' ';
			}
		}

		if (game.showStartScreen)
		{
			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					screenBuffer[x, y] = startScreen[height - y - 1][x];
				}
			}
		}
		else
		{
			if (map != null)
			{
				for (int x = 0; x < width; x++)
				{
					for (int y = 0; y < height; y++)
					{
						screenBuffer[x, y] = baseUI[height - y - 1][x];
					}
				}

				if (map.player.itemsEnabled)
				{
					screenBuffer[104, 3] = (char)((Mathf.FloorToInt (map.player.gold / 1000) % 10) + 48);
					screenBuffer[105, 3] = (char)((Mathf.FloorToInt (map.player.gold / 100) % 10) + 48);
					screenBuffer[106, 3] = (char)((Mathf.FloorToInt (map.player.gold / 10) % 10) + 48);
					screenBuffer[107, 3] = (char)((Mathf.FloorToInt (map.player.gold / 1) % 10) + 48);
				}
				else
				{
					for (int i = 0; i < 11; i++) screenBuffer[97 + i, 3] = ' ';
					for (int i = 0; i < 14; i++) screenBuffer[96 + i, 4] = ' ';
					screenBuffer[110, 4] = '║';
					screenBuffer[95, 4] = '╗';
				}

				if (map.player.xpEnabled)
				{
					screenBuffer[89, 1] = (char)((Mathf.FloorToInt (map.player.xp / 1000) % 10) + 48);
					screenBuffer[90, 1] = (char)((Mathf.FloorToInt (map.player.xp / 100) % 10) + 48);
					screenBuffer[91, 1] = (char)((Mathf.FloorToInt (map.player.xp / 10) % 10) + 48);
					screenBuffer[92, 1] = (char)((Mathf.FloorToInt (map.player.xp / 1) % 10) + 48);

					screenBuffer[104, 1] = (char)((Mathf.FloorToInt (map.player.level / 1000) % 10) + 48);
					screenBuffer[105, 1] = (char)((Mathf.FloorToInt (map.player.level / 100) % 10) + 48);
					screenBuffer[106, 1] = (char)((Mathf.FloorToInt (map.player.level / 10) % 10) + 48);
					screenBuffer[107, 1] = (char)((Mathf.FloorToInt (map.player.level / 1) % 10) + 48);
				}
				else
				{
					for (int i = 0; i < 11; i++)
					{
						screenBuffer[82 + i, 1] = ' ';
						screenBuffer[97 + i, 1] = ' ';
						screenBuffer[97 + i, 1] = ' ';
					}

					for (int i = 0; i < 29; i++)
					{
						screenBuffer[81 + i, 1] = ' ';
						screenBuffer[81 + i, 2] = ' ';
					}

					screenBuffer[80, 2] = '║';
					screenBuffer[110, 2] = '║';
					screenBuffer[95, 0] = '═';
				}

				if (map.player.healthEnabled)
				{

					screenBuffer[89, 3] = (char)((Mathf.FloorToInt (map.player.currentHealth / 1000) % 10) + 48);
					screenBuffer[90, 3] = (char)((Mathf.FloorToInt (map.player.currentHealth / 100) % 10) + 48);
					screenBuffer[91, 3] = (char)((Mathf.FloorToInt (map.player.currentHealth / 10) % 10) + 48);
					screenBuffer[92, 3] = (char)((Mathf.FloorToInt (map.player.currentHealth / 1) % 10) + 48);
				}
				else
				{
					for (int i = 0; i < 11; i++) screenBuffer[82 + i, 3] = ' ';

					for (int i = 0; i < 29; i++)
					{
						screenBuffer[81 + i, 3] = ' ';
						screenBuffer[81 + i, 4] = ' ';
					}

					screenBuffer[80, 4] = '║';
					screenBuffer[110, 4] = '║';
					if (map.player.xpEnabled) screenBuffer[95, 2] = '╦';
				}

				if (map.player.statsEnabled)
				{
					screenBuffer[91, 7] = (char)((Mathf.FloorToInt (map.player.healthStat / 10) % 10) + 48);
					screenBuffer[92, 7] = (char)((Mathf.FloorToInt (map.player.healthStat / 1) % 10) + 48);
					screenBuffer[106, 7] = (char)((Mathf.FloorToInt (map.player.speedStat / 10) % 10) + 48);
					screenBuffer[107, 7] = (char)((Mathf.FloorToInt (map.player.speedStat / 1) % 10) + 48);
					screenBuffer[91, 5] = (char)((Mathf.FloorToInt (map.player.strengthStat / 10) % 10) + 48);
					screenBuffer[92, 5] = (char)((Mathf.FloorToInt (map.player.strengthStat / 1) % 10) + 48);
					screenBuffer[106, 5] = (char)((Mathf.FloorToInt (map.player.intelligenceStat / 10) % 10) + 48);
					screenBuffer[107, 5] = (char)((Mathf.FloorToInt (map.player.intelligenceStat / 1) % 10) + 48);
				}
				else
				{
					for (int i = 0; i < 29; i++)
					{
						screenBuffer[81 + i, 5] = ' ';
						screenBuffer[81 + i, 6] = ' ';
						screenBuffer[81 + i, 7] = ' ';
						screenBuffer[81 + i, 8] = ' ';
					}

					screenBuffer[80, 6] = '║';
					screenBuffer[110, 6] = '║';
					screenBuffer[80, 8] = '║';
					screenBuffer[110, 8] = '║';
					if (map.player.healthEnabled && map.player.itemsEnabled) screenBuffer[95, 4] = '╦';
				}

				int level = 0;
				bool doneUnasigned = false;
				foreach (KeyValuePair<char, string> item in map.game.Abilities)
				{
					if (item.Key != (char)0) screenBuffer[83, height - 2 - level] = char.ToUpper (item.Key);
					else if (doneUnasigned) break;
					else doneUnasigned = true;
					screenBuffer[85, height - 2 - level] = '-';

					for (int c = 0; c < item.Value.Length; c++) screenBuffer[87 + c, height - 2 - level] = item.Value[c];

					level++;
				}

				for (int i = 0; i < logMessages.Length; i++)
				{
					if (logMessages[i] != null && logMessages[i] != "")
					{
						for (int c = 0; c < logMessages[i].Length; c++)
						{
							screenBuffer[c + 2, 8 - i] = logMessages[i][c];
						}
					}
				}

				int offsetX = 0;
				int offsetY = 0;
				if (map.level.canPan && map.player != null)
				{
					offsetX = map.player.position.x - playerPositionX;
					offsetY = map.player.position.y - playerPositionY;
				}

				for (int x = 0; x < mapAreaWidth; x++)
				{
					for (int y = 0; y < mapAreaHeight; y++)
					{
						screenBuffer[x + 1, y + height - mapAreaHeight - 1] = map.GetDisplayChar (x + offsetX, y + offsetY);
					}
				}

				if (!map.player.alive)
				{
					for (int x = 0; x < gameOverUIWidth; x++)
					{
						for (int y = 0; y < gameOverUIHeight; y++)
						{
							screenBuffer[x + 29, gameOverUIHeight - y + 15] = gameOverUI[y][x];
						}
					}

					screenBuffer[43, 23] = (char)((Mathf.FloorToInt (map.player.gold / 1000) % 10) + 48);
					screenBuffer[44, 23] = (char)((Mathf.FloorToInt (map.player.gold / 100) % 10) + 48);
					screenBuffer[45, 23] = (char)((Mathf.FloorToInt (map.player.gold / 10) % 10) + 48);
					screenBuffer[46, 23] = (char)((Mathf.FloorToInt (map.player.gold / 1) % 10) + 48);

					screenBuffer[43, 22] = (char)((Mathf.FloorToInt (map.player.xp / 1000) % 10) + 48);
					screenBuffer[44, 22] = (char)((Mathf.FloorToInt (map.player.xp / 100) % 10) + 48);
					screenBuffer[45, 22] = (char)((Mathf.FloorToInt (map.player.xp / 10) % 10) + 48);
					screenBuffer[46, 22] = (char)((Mathf.FloorToInt (map.player.xp / 1) % 10) + 48);

					screenBuffer[43, 21] = (char)((Mathf.FloorToInt (map.player.level / 1000) % 10) + 48);
					screenBuffer[44, 21] = (char)((Mathf.FloorToInt (map.player.level / 100) % 10) + 48);
					screenBuffer[45, 21] = (char)((Mathf.FloorToInt (map.player.level / 10) % 10) + 48);
					screenBuffer[46, 21] = (char)((Mathf.FloorToInt (map.player.level / 1) % 10) + 48);
				}
			}
		}
		
		// Create the text for the screen
		outputTextBuilder.Clear ();

		for (int y = height - 1; y >= 0; y--)
		{
			for (int x = 0; x < width; x++)
			{
				outputTextBuilder.Append (screenBuffer[x, y]);
			}

			outputTextBuilder.Append (newLine);
		}

		displayText.text = outputTextBuilder.ToString ();
	}

	public void AddLogMessage (string message)
	{
		if (message.Length > maxLogLineLength) message = message.Substring (0, maxLogLineLength);

		for (int i = logLineCount - 2; i >= 0; i--) logMessages[i + 1] = logMessages[i];
		logMessages[0] = message;
	}

	public void SetColour (Color c)
	{
		color = c;
		displayText.color = c;
		int bloomIndex = postProcessing.profile.settings.FindIndex (x => x is Bloom);
		if (bloomIndex < 0) Debug.LogError ("No bloom component on camera post processing");
		else ((Bloom)postProcessing.profile.settings[bloomIndex]).color.value = c;
	}

	public void SetScreenSize (int w, int h)
	{
		width = w;
		height = h;
		screenBuffer = new char[width, height];
	}
}
