﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
	public readonly int x;
	public readonly int y;
	public readonly Map map;
	public readonly bool isWall;
	public readonly bool isDoor;
	public readonly char floorChar;

	public int roomNumber;
	public bool accessible;
	public bool isOpenDoor;

	public Character currentCharacter { get; private set; }

	public List<Item> items = new List<Item> ();

	public Tile right { get; private set; }
	public Tile left { get; private set; }
	public Tile up { get; private set; }
	public Tile down { get; private set; }

	public Tile (int x, int y, Map map, bool isWall, bool isDoor, char floorChar)
	{
		this.x = x;
		this.y = y;
		this.map = map;
		this.isWall = isWall;
		this.isDoor = isDoor;
		this.isOpenDoor = !isDoor;
		this.floorChar = floorChar;
		this.currentCharacter = null;
		this.accessible = false;

		roomNumber = 0;
	}

	public float DistanceTo (Tile other)
	{
		float dx = other.x - x;
		float dy = other.y - y;
		return Mathf.Sqrt ((dx * dx) + (dy * dy));
	}

	public bool TryUseDoor ()
	{
		if (isDoor)
		{
			isOpenDoor = !isOpenDoor;
			return true;
		}

		return false;
	}

	public bool CanUseDoor ()
	{
		return isDoor;
	}

	public void GenerateNeighbours ()
	{
		right = map[x + 1, y + 0];
		left = map[x - 1, y + 0];
		up = map[x + 0, y + 1];
		down = map[x + 0, y - 1];
	}

	public void AddItem (Item item)
	{
		if (!items.Contains (item)) items.Add (item);
	}

	public void RemoveItem (Item item)
	{
		if (items.Contains (item)) items.Remove (item);
	}

	public bool TryAddCharacter (Character c)
	{
		if (isWall || (isDoor && map.player != null && map.player.doorsEnabled && !isOpenDoor)) return false;
		if (currentCharacter == null || currentCharacter == c)
		{
			currentCharacter = c;
			return true;
		}
		else return false;
	}

	public void RemoveCharacter (Character c)
	{
		if (currentCharacter == c) currentCharacter = null;
	}

	public override string ToString ()
	{
		return $"[{x}, {y}]";
	}
}
